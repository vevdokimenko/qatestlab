package com.qatestlab.test;

public class Triangle extends Shape {
    private double sideA;
    private double sideB;
    private double sideC;

    public Triangle(double sideA, double sideB, double sideC, String color) {
        this.sideA = (sideA > 0) ? sideA : getRandomSide();
        this.sideB = (sideB > 0) ? sideB : getRandomSide();
        if (sideC > 0 && (sideC < this.sideA + this.sideB && sideC > Math.abs(this.sideA - this.sideB))) {
            this.sideC = sideC;
        } else {
            this.sideC = getValidSide(this.sideA, this.sideB);
        }
        this.setColor(color);
    }

    public Triangle(double sideA, double sideB, double sideC) {
        this(sideA, sideB, sideC, getRandomColor());
    }

    public Triangle(double sideA, double sideB) {
        this(sideA, sideB, getRandomSide(), getRandomColor());
    }

    public Triangle(double sideA) {
        this(sideA, getRandomSide(), getRandomSide(), getRandomColor());
    }

    public Triangle(String color) {
        this(getRandomSide(), getRandomSide(), getRandomSide(), color);
    }

    public Triangle() {
        this(getRandomSide(), getRandomSide(), getRandomSide(), getRandomColor());
    }

    public double getSideA() {
        return sideA;
    }

    public double getSideB() {
        return sideB;
    }

    public double getSideC() {
        return sideC;
    }

    // Является ли треугольник прямоугольным
    public boolean isRight() {
        boolean result = false;

        double alpha = Math.toDegrees(Math.acos((Math.pow(sideB, 2) + Math.pow(sideC, 2) - Math.pow(sideA, 2)) / (2 * sideB * sideC)));
        double beta = Math.toDegrees(Math.acos((Math.pow(sideA, 2) + Math.pow(sideC, 2) - Math.pow(sideB, 2)) / (2 * sideA * sideC)));
        double gamma = Math.toDegrees(Math.acos((Math.pow(sideA, 2) + Math.pow(sideB, 2) - Math.pow(sideC, 2)) / (2 * sideA * sideB)));

        if (90 == alpha || 90 == beta || 90 == gamma) {
            result = true;
        }

        return result;
    }

    public double getHypotenuse() {
        double max = sideA;
        max = (sideB > max) ? sideB : max;
        max = (sideC > max) ? sideC : max;

        return isRight() ? max : 0;
    }

    @Override
    public void draw() {
        System.out.println("The triangle was drawn.");
    }

    @Override
    public double getArea() {
        double p = (sideA + sideB + sideC) / 2;
        return Math.sqrt(p * (p - sideA) * (p - sideB) * (p - sideC));
    }

    @Override
    public String toString() {
        String type = (isRight()) ? "гипотенуза: " + String.format("%.1f", getHypotenuse()) + " ед." : "не прямоугольный";
        return "Фигура: треугольник, площадь: " +
                String.format("%.1f", getArea()) +
                " кв. ед., " +
                type +
                ", цвет: " +
                getColor();
    }

    // Получение валидной третьей стороны треугольника
    private double getValidSide(double sideA, double sideB) {
        sideC = getRandomSide();
        while (sideC > sideA + sideB || sideC < Math.abs(sideA - sideB)) {
            sideC = getRandomSide();
        }
        return sideC;
    }
}
