package com.qatestlab.test;

import java.util.Random;

/*
Базовый абстрактный класс
*/
public abstract class Shape {
    private double area;
    private String color;

    // Стандартный набор цветов
    static String[] colors = new String[]{
            "Black",
            "Grey",
            "White",
            "Red",
            "Yellow",
            "Green",
            "Blue"
    };

    public abstract void draw();

    public abstract double getArea();

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    // Генератор случайного цвета
    public static String getRandomColor() {
        int index = new Random().nextInt(colors.length);
        return colors[index];
    }

    // Генератор случайно длины стороны
    public static double getRandomSide() {
        return new Random().nextDouble() * 100;
    }
}
