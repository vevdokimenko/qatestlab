package com.qatestlab.test;

public class Square extends Shape {
    private double side;

    public Square(double side, String color) {
        this.side = (side > 0) ? side : getRandomSide();
        this.setColor(color);
    }

    public Square(double side) {
        this(side, getRandomColor());
    }

    public Square(String color) {
        this(getRandomSide(), color);
    }

    public Square() {
        this(getRandomSide(), getRandomColor());
    }

    public double getSide() {
        return side;
    }

    @Override
    public void draw() {
        System.out.println("The square was drawn.");
    }

    @Override
    public double getArea() {
        return Math.pow(side, 2);
    }

    @Override
    public String toString() {
        return "Фигура: квадрат, площадь: " +
                String.format("%.1f", getArea()) +
                " кв. ед., длина стороны: " +
                String.format("%.1f", getSide()) +
                " ед., цвет: " +
                getColor();
    }
}
