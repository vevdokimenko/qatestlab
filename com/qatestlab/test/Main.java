package com.qatestlab.test;

/*
1.
Дано объекты-фигуры следующих видов: квадрат, треугольник, круг, трапеция.
Каждую фигуру можно нарисовать, получить ее площадь и цвет.
Также фигуры имеют уникальные методы, например: вернуть радиус, длину гипотенузы, длину стороны и т. д.

2.
Нам необходимо сгенерировать случайный набор фигур, количество объектов в наборе также заранее неизвестно.
После генерации массива нужно вывести весь список объектов, которые у нас имеются,
например:

Фигура: квадрат, площадь: 25 кв. ед., длина стороны: 5 ед., цвет: синий
Фигура: треугольник, площадь: 12,5 кв.ед., гипотенуза: 7.1 ед., цвет: желтый
*/

import java.util.ArrayList;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        // Набор фигур
        ArrayList<Shape> list = new ArrayList<>();

        for (int i = 0; i < new Random().nextInt(11) + 1; i++) { // Цикл от 0 до случайного числа (от 1 до 10).
            int shapeType = new Random().nextInt(4); // Генерируем случайный номер фигуры
            switch (shapeType) {
                case 0:
                    list.add(new Square());
                    break;
                case 1:
                    list.add(new Triangle());
                    break;
                case 2:
                    list.add(new Circle());
                    break;
                case 3:
                    list.add(new Trapeze());
                    break;
            }
        }

        for (Shape shape : list) {
            System.out.println(shape);
        }
    }
}
