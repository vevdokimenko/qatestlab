package com.qatestlab.test;

public class Trapeze extends Shape {
    private double topBase;
    private double bottomBase;
    private double height;

    public Trapeze(double topBase, double bottomBase, double height, String color) {
        this.topBase = (topBase > 0) ? topBase : getRandomSide();
        this.bottomBase = (bottomBase > 0) ? bottomBase : getRandomSide();
        this.height = (height > 0) ? height : getRandomSide();
        this.setColor(color);
    }

    public Trapeze(double topBase, double bottomBase, double height) {
        this(topBase, bottomBase, height, getRandomColor());
    }

    public Trapeze(double topBase, double bottomBase) {
        this(topBase, bottomBase, getRandomSide(), getRandomColor());
    }

    public Trapeze(double topBase) {
        this(topBase, topBase, getRandomSide(), getRandomColor());
    }

    public Trapeze(String color) {
        this(getRandomSide(), getRandomSide() * 1.5, getRandomSide(), color);
    }

    public Trapeze() {
        this(getRandomSide(), getRandomSide() * 1.5, getRandomSide(), getRandomColor());
    }

    public double getTopBase() {
        return topBase;
    }

    public double getBottomBase() {
        return bottomBase;
    }

    public double getHeight() {
        return height;
    }

    public double getMidLine() {
        return 0.5 * (topBase + bottomBase);
    }

    @Override
    public void draw() {
        System.out.println("The trapeze was drawn.");
    }

    @Override
    public double getArea() {
        return 0.5 * (topBase + bottomBase) * height;
    }

    @Override
    public String toString() {
        return "Фигура: трапеция, площадь: " +
                String.format("%.1f", getArea()) +
                " кв. ед., средняя линия: " +
                String.format("%.1f", getMidLine()) +
                " ед., цвет: " +
                getColor();
    }
}
