package com.qatestlab.test;

public class Circle extends Shape {
    private double radius;

    public Circle(double radius, String color) {
        this.radius = (radius > 0) ? radius : getRandomSide();
        this.setColor(color);
    }

    public Circle(double radius) {
        this(radius, getRandomColor());
    }

    public Circle(String color) {
        this(getRandomSide(), color);
    }

    public Circle() {
        this(getRandomSide(), getRandomColor());
    }

    public double getRadius() {
        return radius;
    }

    @Override
    public void draw() {
        System.out.println("The circle was drawn.");
    }

    @Override
    public double getArea() {
        return Math.PI * Math.pow(radius, 2);
    }

    @Override
    public String toString() {
        return "Фигура: круг, площадь: " +
                String.format("%.1f", getArea()) +
                " кв. ед., радиус: " +
                String.format("%.1f", getRadius()) +
                " ед., цвет: " +
                getColor();
    }
}
